#!/bin/bash
set -e

# create ros group and ros user matching uid and gid of the host user
id -u ros &> /dev/null || ( # if user ros does not exist
  groupadd -g $HOST_GID ros 
  useradd -m -u $HOST_UID -g $HOST_GID ros 
  adduser ros sudo
  chown -R ros:ros /home/ros
) &> /dev/null

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
exec "$@"
